from conans import ConanFile, CMake, tools

class MiniaudioConan(ConanFile):
    name = "miniaudio"
    version = "0.10.31"
    license = "MIT"
    topics = ("audio", "header-only", "playback", "decoding")
    settings = "os"
    url = "https://bitbucket.org/toge/conan-miniaudio"
    description = "Single file audio playback and capture library."
    no_copy_source = True

    def source(self):
        self.run("git clone https://github.com/dr-soft/miniaudio/")
        self.run("cd miniaudio && git checkout 53512c39b79ee284058e139604f358fd8a3489cd")

    def package(self):
        self.copy("miniaudio.h", dst="include/miniaudio", src="miniaudio")
        self.copy("*.h", dst="include/miniaudio/extras", src="miniaudio/extras")

    def package_info(self):
        self.info.header_only()
        if self.settings.os == "Linux":
            self.cpp_info.libs = ["dl", "m", "pthread"]
        elif self.settings.os == "Windows":
            self.cpp_info.libs = []
        elif self.settings.os == "Macos":
            self.cpp_info.defines.extend(['__WXMAC__', '__WXOSX__', '__WXOSX_COCOA__'])
            for framework in ['CoreFoundation',
                              'CoreAudio',
                              'AudioToolbox',
                              ]:
                self.cpp_info.exelinkflags.append('-framework %s' % framework)
            self.cpp_info.sharedlinkflags = self.cpp_info.exelinkflags
        else:
            self.cpp_info.libs = []
