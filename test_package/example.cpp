#include <iostream>

#define MINIAUDIO_IMPLEMENTATION
#include "miniaudio/miniaudio.h"

int main(int argc, const char** argv) {
    ma_context context;
    if (ma_context_init(nullptr, 0, nullptr, &context) != MA_SUCCESS) {
        std::cerr << "Failed to initialize context." << std::endl;
        return 1;
    }

    return 0;
}
